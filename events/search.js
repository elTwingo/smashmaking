import matchmaking from "./matchmaking/index.js";
import match from './match.js';

const SEARCH_INTERVAL_IN_SECOND = 3;
const SEARCH_MAX_DELAY_IN_SECOND = 10;

export default async (playerInteraction, queue, client) => {
    let timeout = false;

    const search = () => {
        setTimeout(async () => {
            console.log(timeout)
            const result = matchmaking.found(queue)
            if (queue.includes(playerInteraction) && timeout) {
                matchmaking.notFound(playerInteraction, queue)
                await playerInteraction.editReply({ content: 'not Found', ephemeral: true })
            } else if (result.length) {
                await match(result, client);
                timeout = true;
            } else if (!timeout) {
                await search()
            }
        }, SEARCH_INTERVAL_IN_SECOND * 1000);
    }


    await playerInteraction.deferReply({ ephemeral: true });
    matchmaking.addPlayer(playerInteraction, queue);

    setTimeout(() => timeout = true, SEARCH_MAX_DELAY_IN_SECOND * 1000)
    search();

    
};