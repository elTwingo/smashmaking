import { ChannelType, ButtonBuilder, ButtonStyle, ActionRowBuilder } from 'discord.js';

export default async ([player1, player2], client) => {
    await player1.editReply('trouvé');
    await player2.editReply('trouvé');
    await player1.followUp({ content: ' <@' + player1.user.id + '> tu joues contre ' + ' <@' + player2.user.id + '>', ephemeral: true });
    await player2.followUp({ content: ' <@' + player2.user.id + '> tu joues contre ' + ' <@' + player1.user.id + '>', ephemeral: true });
    const thread = await client.channels.cache.get(player1.channelId).threads.create({
        name: 'FIGHT !',
        autoArchiveDuration: 60,
        type: ChannelType.PrivateThread,
      });
      await thread.members.add(player1.user.id);
      await thread.members.add(player2.user.id);
      client.channels.cache.get(thread.id).send(`${player1.user.username} VS ${player2.user.username}`);
      const arenaUser = [player1, player2][Math.floor(Math.random() * 2)]
      client.channels.cache.get(thread.id).send(`${arenaUser.user.username} doit créer l'arène: le match est un bo3`);
      const winner1 = new ButtonBuilder()
        .setCustomId(`win_${player1.user.id}_vs_${player2.user.id}`)
        .setLabel(player1.user.username)
        .setStyle(ButtonStyle.Primary);
    
        const winner2 = new ButtonBuilder()
        .setCustomId(`win_${player2.user.id}_vs_${player1.user.id}`)
        .setLabel(player2.user.username)
        .setStyle(ButtonStyle.Primary);
    
      const row = new ActionRowBuilder()
        .addComponents(winner1, winner2);
    
      client.channels.cache.get(thread.id).send({ content: `Qui est le gagnant ?`, components: [row]});
};