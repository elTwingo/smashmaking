import addPlayer from "./addPlayer.js";
import found from "./found.js";
import notFound from "./notFound.js";

export default { addPlayer, found, notFound };