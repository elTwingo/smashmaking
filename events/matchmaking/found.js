export default (queue) => {
    if (queue.length <= 1) return [];
    const remainingPlayers = [...queue]
    const player1 = remainingPlayers[Math.floor(Math.random() * remainingPlayers.length)]
    remainingPlayers.splice(remainingPlayers.indexOf(player1), 1);
    const player2 = remainingPlayers[Math.floor(Math.random() * remainingPlayers.length)]
    remainingPlayers.splice(remainingPlayers.indexOf(player2), 1);
    queue.splice(queue.indexOf(player1), 1);
    queue.splice(queue.indexOf(player2), 1);
    return [player1, player2];
};