import { REST, Routes, Client, GatewayIntentBits, ButtonBuilder, ButtonStyle, ActionRowBuilder } from 'discord.js';
import 'dotenv/config';
import search from './events/search.js';
//import { cronJob } from 'cron';

const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent] });
const { TOKEN, BOT_CLIENT_ID } = process.env;

const queue = [];

const commands = [
  {
    name: 'initladder',
    description: 'initLadder',
  }
];


client.on('interactionCreate', async (interaction) => {
  if (interaction.isButton() && interaction.customId.startsWith('win_')) {
    const array = interaction.customId.split("_")
    const winner =  array[1];
    const looser = array[3];
    await interaction.reply('Résultats enregistrés !')
    client.channels.cache.get(interaction.channelId).delete();
    console.log('winner', winner)
    console.log('looser', looser)
  }

  if (interaction.isButton() && interaction.customId === 'search') await search(interaction, queue, client)

  if (!interaction.isChatInputCommand()) return;

  if (interaction.commandName === 'initladder') {
    const search = new ButtonBuilder()
      .setCustomId('search')
      .setLabel('Search')
      .setStyle(ButtonStyle.Primary);

    const row = new ActionRowBuilder()
      .addComponents(search);

    await interaction.reply({
      content: `Salutu c est ici le smashmaking bouffon`,
      components: [row],
    });
  }
});

//client.on('ready',() => new CronJob(
//    '*/2 * * * * *',
//    async () => await search(queue, client),
//    null,
//    true,
//    'Europe/Paris'
//)).start()

client.on('ready', async () => {
  const rest = new REST({ version: '10' }).setToken(TOKEN);
  try {
    await rest.put(Routes.applicationCommands(BOT_CLIENT_ID), { body: commands });
    console.log('Ready!');
  } catch (error) {
    console.error(error);
  }
})

client.login(TOKEN);
